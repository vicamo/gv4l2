# v4l2-relayd

## Table of contents
* [Copyright](#copyright)

```
$ ./autogen.sh && make
$ export G_MESSAGES_DEBUG=all
$ ./src/v4l2-relayd -d -r 16 -o /dev/video1
```

## Copyright

Icons made by [Freepik](https://www.flaticon.com/authors/freepik "Freepik") from [www.flaticon.com](https://www.flaticon.com/ "Flaticon").