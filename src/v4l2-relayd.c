/* v4l2-relayd - V4L2 camera streaming relay daemon
 * Copyright (C) 2020 Canonical Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details:
 */

#if defined (HAVE_CONFIG_H)
#include "config.h"
#endif

#include <errno.h>
#include <unistd.h>

#include <glib.h>

#include "gv4l2/gv4l2.h"

static GMainLoop *main_loop = NULL;

static gboolean opt_background = FALSE;
static gboolean opt_debug = FALSE;
static gboolean opt_version = FALSE;
static gchar *opt_capture = NULL;
static gchar *opt_output = NULL;
static gint opt_width = 640;
static gint opt_height = 480;
static guint32 opt_pixelformat = 0;
static gint opt_reqbuf_count = 4;

static gboolean
parse_pixelformat (const gchar  *option_name,
                   const gchar  *value,
                   gpointer      data,
                   GError      **error)
{
  if (g_str_has_prefix (value, "0x")) {
    gchar *end = NULL;
    guint64 result = g_ascii_strtoull (value + 2, &end, 16);
    if (result && result < G_MAXUINT32 && *end == '\0') {
      opt_pixelformat = result;
      return TRUE;
    }
  } else if (strlen (value) == 4) {
    opt_pixelformat = v4l2_fourcc (value[0], value[1], value[2], value[3]);
    return TRUE;
  }

  g_set_error (error, G_OPTION_ERROR, G_OPTION_ERROR_BAD_VALUE,
               "Invalid pixelformat");

  return FALSE;
}

static const GOptionEntry opt_entries[] =
{
  { "background", 'D', G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE,
    &opt_background, "Run in the background", NULL },
  { "debug",      'd', G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE,
    &opt_debug, "Print debugging information", NULL },
  { "version",    'v', G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE,
    &opt_version, "Show version", NULL },
  { "capture",    'c', G_OPTION_FLAG_NONE, G_OPTION_ARG_FILENAME,
    &opt_capture, "Specify capturing device", NULL},
  { "output",     'o', G_OPTION_FLAG_NONE, G_OPTION_ARG_FILENAME,
    &opt_output, "Specify output device", NULL},
  { "width",      'W', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT,
    &opt_width, "Specify output width", NULL},
  { "height",     'H', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT,
    &opt_height, "Specify output height", NULL},
  { "pixelformat", 'f', G_OPTION_FLAG_NONE, G_OPTION_ARG_CALLBACK,
    &parse_pixelformat, "Specify output pixelformat in either heximal number "
                        "'0x01234567' or four characters 'ABCD'.", NULL},
  { "reqbuf-count", 'r', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT,
    &opt_reqbuf_count, "Specify number of buffers used in VIDIOC_REQBUFS", NULL},
  { NULL }
};

static void
parse_args (int   argc,
            char *argv[])
{
  GError *error = NULL;
  GOptionContext *context;

  context = g_option_context_new ("- test tree model performance");
  g_option_context_add_main_entries (context, opt_entries, NULL);
  g_option_context_set_help_enabled (context, TRUE);

  if (!g_option_context_parse (context, &argc, &argv, &error))
  {
    if (error != NULL) {
      g_printerr ("option parsing failed: %s\n", error->message);
      g_error_free (error);
    } else
      g_printerr ("option parsing failed: unknown error\n");

    g_option_context_free (context);
    exit (1);
  }

  g_option_context_free (context);

  if (opt_version) {
    g_print ("%s (%s)\n", g_get_prgname (), GV4L2_VERSION_STR);
    exit (0);
  }

  if (opt_debug) {
    g_log_set_handler (G_LOG_DOMAIN, G_LOG_LEVEL_INFO,
                       g_log_default_handler, NULL);
    g_log_set_handler (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG,
                       g_log_default_handler, NULL);
  }

  if (opt_background) {
    if (daemon (0, 0) < 0) {
      int saved_errno;

      saved_errno = errno;
      g_printerr ("Could not daemonize: %s [error %u]\n",
                  g_strerror (saved_errno), saved_errno);
      exit (1);
    }
  }
}

static int
setup_output (GV4l2Device  *device,
              GError      **error)
{
  struct v4l2_format fmt;
  int ret;

#define MUST_SUCCEED(x) { ret = (x); if (ret < 0) return ret; }

  MUST_SUCCEED(g_v4l2_device_subscribe_event (device, G_V4L2_EVENT_CLIENT_USAGE,
                                              0, V4L2_EVENT_SUB_FL_SEND_INITIAL,
                                              error))

  /* Clear previous streaming buffers */
  MUST_SUCCEED(g_v4l2_streamable_reset (G_V4L2_STREAMABLE (device),
                                        G_V4L2_BUF_TYPE_VIDEO_OUTPUT,
                                        G_V4L2_MEMORY_MMAP, error))

  /* Get if G_V4L2_BUF_TYPE_VIDEO_OUTPUT is supported */
  MUST_SUCCEED(g_v4l2_device_get_format (device, G_V4L2_BUF_TYPE_VIDEO_OUTPUT,
                                         &fmt, error))

  /* Set format */
  memset (&fmt, 0, sizeof(fmt));
  fmt.type = G_V4L2_BUF_TYPE_VIDEO_OUTPUT;
  fmt.fmt.pix.width = opt_width;
  fmt.fmt.pix.height = opt_height;
  fmt.fmt.pix.pixelformat = opt_pixelformat;
  fmt.fmt.pix.field = G_V4L2_FIELD_ANY;
  fmt.fmt.pix.colorspace = G_V4L2_COLORSPACE_DEFAULT;
  MUST_SUCCEED(g_v4l2_device_set_format (device, &fmt, error))

  /* Request buffer allocation */
  MUST_SUCCEED(g_v4l2_device_start_streaming (device, opt_reqbuf_count,
                                              G_V4L2_BUF_TYPE_VIDEO_OUTPUT,
                                              G_V4L2_MEMORY_MMAP, error));

#undef MUST_SUCCEED

  return 0;
}

int
main (int   argc,
      char *argv[])
{
  GV4l2Device *output;
  GV4l2Capabilities caps;
  GError *error = NULL;
  guint version;
  gchar *caps_str;
  int ret;

  parse_args (argc, argv);

  main_loop = g_main_loop_new (NULL, FALSE);

  output = g_v4l2_device_new_from_filename (opt_output,
      G_V4L2_DEVICE_INIT_V4L2LOOPBACK_WORKARROUND, &error);
  if (output == NULL) {
    g_printerr ("%s\n", error->message);
    g_error_free (error);
    return -1;
  }

  caps = g_v4l2_device_get_capabilities (output);
  g_message ("Driver name   : %s", g_v4l2_device_get_driver_name (output));
  g_message ("Card type     : %s", g_v4l2_device_get_card_name (output));
  g_message ("Bus info      : %s", g_v4l2_device_get_bus_info (output));
  version = g_v4l2_device_get_driver_version (output);
  g_message ("Driver version: %u.%u.%u (0x%X)",
             (version >> 16) & 0xFF, (version >> 8) & 0xFF,
             version & 0xFF, version);

  caps_str = g_v4l2_capabilities_build_string_from_mask (caps);
  g_message ("Capabilities  : 0x%X - %s", caps, caps_str);
  g_free (caps_str);

  if (!(G_V4L2_CAP_VIDEO_OUTPUT & caps)) {
    g_printerr ("device does not support type video-output\n");
    return -1;
  }

  if (!(G_V4L2_CAP_STREAMING & caps)) {
    g_printerr ("device does not support streaming\n");
    return -1;
  }

  ret = setup_output (output, &error);
  if (ret < 0) {
    g_printerr ("%s\n", error->message);
    g_error_free (error);

    g_object_unref (output);
    g_main_loop_unref (main_loop);
    return -1;
  }

  g_main_loop_run (main_loop);
  while (g_main_context_pending (NULL)) {
    if (!g_main_context_iteration (NULL, FALSE))
      break;
  }

  g_object_unref (output);

  g_main_loop_unref (main_loop);
  main_loop = NULL;

  return 0;
}
