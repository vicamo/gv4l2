/* gv4l2 - V4L2 abstraction library using GLib
 *
 * Copyright (C) 2020 Canonical Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details:
 */

#pragma once

#if !defined (__GV4L2_GV4L2_H_INSIDE__) && !defined (LIBGV4L2_COMPILATION)
#error "Only <gv4l2/gv4l2.h> can be included directly."
#endif

G_BEGIN_DECLS

typedef struct _GV4l2Device GV4l2Device;
typedef struct _GV4l2Streamable GV4l2Streamable;

G_END_DECLS
