/* gv4l2 - V4L2 abstraction library using GLib
 *
 * Copyright (C) 2020 Canonical Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details:
 */

#pragma once

#if !defined (__GV4L2_GV4L2_H_INSIDE__) && !defined (LIBGV4L2_COMPILATION)
#error "Only <gv4l2/gv4l2.h> can be included directly."
#endif

#include <linux/videodev2.h>

#include <glib.h>

G_BEGIN_DECLS

typedef enum /*< flags,prefix=G_V4L2_CAP,underscore_name=g_v4l2_capabilities >*/
{
  G_V4L2_CAP_VIDEO_CAPTURE              = 0x00000001 /* V4L2_CAP_VIDEO_CAPTURE */,
  G_V4L2_CAP_VIDEO_OUTPUT               = 0x00000002 /* V4L2_CAP_VIDEO_OUTPUT */,
  G_V4L2_CAP_VIDEO_OVERLAY              = 0x00000004 /* V4L2_CAP_VIDEO_OVERLAY */,
  G_V4L2_CAP_VBI_CAPTURE                = 0x00000010 /* V4L2_CAP_VBI_CAPTURE */,
  G_V4L2_CAP_VBI_OUTPUT                 = 0x00000020 /* V4L2_CAP_VBI_OUTPUT */,
  G_V4L2_CAP_SLICED_VBI_CAPTURE         = 0x00000040 /* V4L2_CAP_SLICED_VBI_CAPTURE */,
  G_V4L2_CAP_SLICED_VBI_OUTPUT          = 0x00000080 /* V4L2_CAP_SLICED_VBI_OUTPUT */,
  G_V4L2_CAP_RDS_CAPTURE                = 0x00000100 /* V4L2_CAP_RDS_CAPTURE */,
  G_V4L2_CAP_VIDEO_OUTPUT_OVERLAY       = 0x00000200 /* V4L2_CAP_VIDEO_OUTPUT_OVERLAY */,
  G_V4L2_CAP_HW_FREQ_SEEK               = 0x00000400 /* V4L2_CAP_HW_FREQ_SEEK */,
  G_V4L2_CAP_RDS_OUTPUT                 = 0x00000800 /* V4L2_CAP_RDS_OUTPUT */,
  G_V4L2_CAP_VIDEO_CAPTURE_MPLANE       = 0x00001000 /* V4L2_CAP_VIDEO_CAPTURE_MPLANE */,
  G_V4L2_CAP_VIDEO_OUTPUT_MPLANE        = 0x00002000 /* V4L2_CAP_VIDEO_OUTPUT_MPLANE */,
  G_V4L2_CAP_VIDEO_M2M_MPLANE           = 0x00004000 /* V4L2_CAP_VIDEO_M2M_MPLANE */,
  G_V4L2_CAP_VIDEO_M2M                  = 0x00008000 /* V4L2_CAP_VIDEO_M2M */,
  G_V4L2_CAP_TUNER                      = 0x00010000 /* V4L2_CAP_TUNER */,
  G_V4L2_CAP_AUDIO                      = 0x00020000 /* V4L2_CAP_AUDIO */,
  G_V4L2_CAP_RADIO                      = 0x00040000 /* V4L2_CAP_RADIO */,
  G_V4L2_CAP_MODULATOR                  = 0x00080000 /* V4L2_CAP_MODULATOR */,
  G_V4L2_CAP_SDR_CAPTURE                = 0x00100000 /* V4L2_CAP_SDR_CAPTURE */,
  G_V4L2_CAP_EXT_PIX_FORMAT             = 0x00200000 /* V4L2_CAP_EXT_PIX_FORMAT */,
  G_V4L2_CAP_SDR_OUTPUT                 = 0x00400000 /* V4L2_CAP_SDR_OUTPUT */,
  G_V4L2_CAP_META_CAPTURE               = 0x00800000 /* V4L2_CAP_META_CAPTURE */,
  G_V4L2_CAP_READWRITE                  = 0x01000000 /* V4L2_CAP_READWRITE */,
  G_V4L2_CAP_ASYNCIO                    = 0x02000000 /* V4L2_CAP_ASYNCIO */,
  G_V4L2_CAP_STREAMING                  = 0x04000000 /* V4L2_CAP_STREAMING */,
  G_V4L2_CAP_META_OUTPUT                = 0x08000000 /* V4L2_CAP_META_OUTPUT */,
  G_V4L2_CAP_TOUCH                      = 0x10000000 /* V4L2_CAP_TOUCH */,
  G_V4L2_CAP_IO_MC                      = 0x20000000 /* V4L2_CAP_IO_MC */,
  G_V4L2_CAP_DEVICE_CAPS                = 0x80000000 /* V4L2_CAP_DEVICE_CAPS */,
} GV4l2Capabilities;

typedef enum /*< enum,prefix=G_V4L2_BUF_TYPE,underscore_name=g_v4l2_buffer_types >*/
{
  G_V4L2_BUF_TYPE_VIDEO_CAPTURE         = 1 /* V4L2_BUF_TYPE_VIDEO_CAPTURE */,
  G_V4L2_BUF_TYPE_VIDEO_OUTPUT          = 2 /* V4L2_BUF_TYPE_VIDEO_OUTPUT */,
  G_V4L2_BUF_TYPE_VIDEO_OVERLAY         = 3 /* V4L2_BUF_TYPE_VIDEO_OVERLAY */,
  G_V4L2_BUF_TYPE_VBI_CAPTURE           = 4 /* V4L2_BUF_TYPE_VBI_CAPTURE */,
  G_V4L2_BUF_TYPE_VBI_OUTPUT            = 5 /* V4L2_BUF_TYPE_VBI_OUTPUT */,
  G_V4L2_BUF_TYPE_SLICED_VBI_CAPTURE    = 6 /* V4L2_BUF_TYPE_SLICED_VBI_CAPTURE */,
  G_V4L2_BUF_TYPE_SLICED_VBI_OUTPUT     = 7 /* V4L2_BUF_TYPE_SLICED_VBI_OUTPUT */,
  G_V4L2_BUF_TYPE_VIDEO_OUTPUT_OVERLAY  = 8 /* V4L2_BUF_TYPE_VIDEO_OUTPUT_OVERLAY */,
  G_V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE  = 9 /* V4L2_BUF_TYPE_VIDEO_CAPTURE_MPLANE */,
  G_V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE   = 10 /* V4L2_BUF_TYPE_VIDEO_OUTPUT_MPLANE */,
  G_V4L2_BUF_TYPE_SDR_CAPTURE           = 11 /* V4L2_BUF_TYPE_SDR_CAPTURE */,
  G_V4L2_BUF_TYPE_SDR_OUTPUT            = 12 /* V4L2_BUF_TYPE_SDR_OUTPUT */,
  G_V4L2_BUF_TYPE_META_CAPTURE          = 13 /* V4L2_BUF_TYPE_META_CAPTURE */,
  G_V4L2_BUF_TYPE_META_OUTPUT           = 14 /* V4L2_BUF_TYPE_META_OUTPUT */,
} GV4l2BufferTypes;

typedef enum /*< flags,prefix=G_V4L2_BUF_CAP_SUPPORTS,underscore_name=g_v4l2_buffer_capabilities >*/
{
  G_V4L2_BUF_CAP_SUPPORTS_MMAP                  = 0x00000001 /* V4L2_BUF_CAP_SUPPORTS_MMAP */,
  G_V4L2_BUF_CAP_SUPPORTS_USERPTR               = 0x00000002 /* V4L2_BUF_CAP_SUPPORTS_USERPTR */,
  G_V4L2_BUF_CAP_SUPPORTS_DMABUF                = 0x00000004 /* V4L2_BUF_CAP_SUPPORTS_DMABUF */,
  G_V4L2_BUF_CAP_SUPPORTS_REQUESTS              = 0x00000008 /* V4L2_BUF_CAP_SUPPORTS_REQUESTS */,
  G_V4L2_BUF_CAP_SUPPORTS_ORPHANED_BUFS         = 0x00000010 /* V4L2_BUF_CAP_SUPPORTS_ORPHANED_BUFS */,
  G_V4L2_BUF_CAP_SUPPORTS_M2M_HOLD_CAPTURE_BUF  = 0x00000020 /* V4L2_BUF_CAP_SUPPORTS_M2M_HOLD_CAPTURE_BUF */,
  G_V4L2_BUF_CAP_SUPPORTS_MMAP_CACHE_HINTS      = 0x00000040 /* V4L2_BUF_CAP_SUPPORTS_MMAP_CACHE_HINTS */,
} GV4l2BufferCapabilities;

typedef enum /*< enum,prefix=G_V4L2_MEMORY,underscore_name=g_v4l2_memory_types >*/
{
  G_V4L2_MEMORY_MMAP    = 1 /* V4L2_MEMORY_MMAP */,
  G_V4L2_MEMORY_USERPTR = 2 /* V4L2_MEMORY_USERPTR */,
  G_V4L2_MEMORY_OVERLAY = 3 /* V4L2_MEMORY_OVERLAY */,
  G_V4L2_MEMORY_DMABUF  = 4 /* V4L2_MEMORY_DMABUF */,
} GV4l2MemoryTypes;

typedef enum /*< enum,prefix=G_V4L2_FIELD,underscore_name=g_v4l2_field_orders >*/
{
  G_V4L2_FIELD_ANY              = 0 /* V4L2_FIELD_ANY */,
  G_V4L2_FIELD_NONE             = 1 /* V4L2_FIELD_NONE */,
  G_V4L2_FIELD_TOP              = 2 /* V4L2_FIELD_TOP */,
  G_V4L2_FIELD_BOTTOM           = 3 /* V4L2_FIELD_BOTTOM */,
  G_V4L2_FIELD_INTERLACED       = 4 /* V4L2_FIELD_INTERLACED */,
  G_V4L2_FIELD_SEQ_TB           = 5 /* V4L2_FIELD_SEQ_TB */,
  G_V4L2_FIELD_SEQ_BT           = 6 /* V4L2_FIELD_SEQ_BT */,
  G_V4L2_FIELD_ALTERNATE        = 7 /* V4L2_FIELD_ALTERNATE */,
  G_V4L2_FIELD_INTERLACED_TB    = 8 /* V4L2_FIELD_INTERLACED_TB */,
  G_V4L2_FIELD_INTERLACED_BT    = 9 /* V4L2_FIELD_INTERLACED_BT */,
} GV4l2FieldOrders;

typedef enum /*< enum,prefix=G_V4L2_COLORSPACE,underscore_name=g_v4l2_colorspaces >*/
{
  G_V4L2_COLORSPACE_DEFAULT             = 0 /* V4L2_COLORSPACE_DEFAULT */,
  G_V4L2_COLORSPACE_SMPTE170M           = 1 /* V4L2_COLORSPACE_SMPTE170M */,
  G_V4L2_COLORSPACE_SMPTE240M           = 2 /* V4L2_COLORSPACE_SMPTE240M */,
  G_V4L2_COLORSPACE_REC709              = 3 /* V4L2_COLORSPACE_REC709 */,
  /* Deprecated: V4L2_COLORSPACE_BT878 */
  G_V4L2_COLORSPACE_470_SYSTEM_M        = 5 /* V4L2_COLORSPACE_470_SYSTEM_M */,
  G_V4L2_COLORSPACE_470_SYSTEM_BG       = 6 /* V4L2_COLORSPACE_470_SYSTEM_BG */,
  G_V4L2_COLORSPACE_JPEG                = 7 /* V4L2_COLORSPACE_JPEG */,
  G_V4L2_COLORSPACE_SRGB                = 8 /* V4L2_COLORSPACE_SRGB */,
  G_V4L2_COLORSPACE_OPRGB               = 9 /* V4L2_COLORSPACE_ADOBERGB/V4L2_COLORSPACE_OPRGB */,
  G_V4L2_COLORSPACE_BT2020              = 10 /* V4L2_COLORSPACE_BT2020 */,
  G_V4L2_COLORSPACE_RAW                 = 11 /* V4L2_COLORSPACE_RAW */,
  G_V4L2_COLORSPACE_DCI_P3              = 12 /* V4L2_COLORSPACE_DCI_P3 */,
} GV4l2Colorspaces;

typedef enum /*< enum,prefix=G_V4L2_EVENT,underscore_name=g_v4l2_events >*/
{
  G_V4L2_EVENT_ALL              = 0 /* V4L2_EVENT_ALL */,
  G_V4L2_EVENT_VSYNC            = 1 /* V4L2_EVENT_VSYNC */,
  G_V4L2_EVENT_EOS              = 2 /* V4L2_EVENT_EOS */,
  G_V4L2_EVENT_CTRL             = 3 /* V4L2_EVENT_CTRL */,
  G_V4L2_EVENT_FRAME_SYNC       = 4 /* V4L2_EVENT_FRAME_SYNC */,
  G_V4L2_EVENT_SOURCE_CHANGE    = 5 /* V4L2_EVENT_SOURCE_CHANGE */,
  G_V4L2_EVENT_MOTION_DET       = 6 /* V4L2_EVENT_MOTION_DET */,

  G_V4L2_EVENT_PRIVATE_START    = 0x08000000 /* V4L2_EVENT_PRIVATE_START */, /*< skip >*/
  G_V4L2_EVENT_CLIENT_USAGE     = G_V4L2_EVENT_PRIVATE_START,
} GV4l2Events;

struct g_v4l2_event_client_usage {
  __u32 count;
};

/* 7.3. ioctl VIDIOC_CREATE_BUFS */
/* 7.4. ioctl VIDIOC_CROPCAP */
/* 7.5. ioctl VIDIOC_DBG_G_CHIP_INFO */
/* 7.6. ioctl VIDIOC_DBG_G_REGISTER, VIDIOC_DBG_S_REGISTER */
/* 7.7. ioctl VIDIOC_DECODER_CMD, VIDIOC_TRY_DECODER_CMD */
/* 7.8. ioctl VIDIOC_DQEVENT */

int g_v4l2_ioctl_dequeue_event (gint                fd,
                                struct v4l2_event  *event,
                                GError            **error);

/* 7.9. ioctl VIDIOC_DV_TIMINGS_CAP, VIDIOC_SUBDEV_DV_TIMINGS_CAP */
/* 7.10. ioctl VIDIOC_ENCODER_CMD, VIDIOC_TRY_ENCODER_CMD */
/* 7.11. ioctl VIDIOC_ENUMAUDIO */
/* 7.12. ioctl VIDIOC_ENUMAUDOUT */
/* 7.13. ioctl VIDIOC_ENUM_DV_TIMINGS, VIDIOC_SUBDEV_ENUM_DV_TIMINGS */
/* 7.14. ioctl VIDIOC_ENUM_FMT */
/* 7.15. ioctl VIDIOC_ENUM_FRAMESIZES */
/* 7.16. ioctl VIDIOC_ENUM_FRAMEINTERVALS */
/* 7.17. ioctl VIDIOC_ENUM_FREQ_BANDS */
/* 7.18. ioctl VIDIOC_ENUMINPUT */
/* 7.19. ioctl VIDIOC_ENUMOUTPUT */
/* 7.20. ioctl VIDIOC_ENUMSTD, VIDIOC_SUBDEV_ENUMSTD */
/* 7.21. ioctl VIDIOC_EXPBUF */
/* 7.22. ioctl VIDIOC_G_AUDIO, VIDIOC_S_AUDIO */
/* 7.23. ioctl VIDIOC_G_AUDOUT, VIDIOC_S_AUDOUT */
/* 7.24. ioctl VIDIOC_G_CROP, VIDIOC_S_CROP */
/* 7.25. ioctl VIDIOC_G_CTRL, VIDIOC_S_CTRL */
/* 7.26. ioctl VIDIOC_G_DV_TIMINGS, VIDIOC_S_DV_TIMINGS */
/* 7.27. ioctl VIDIOC_G_EDID, VIDIOC_S_EDID, VIDIOC_SUBDEV_G_EDID, VIDIOC_SUBDEV_S_EDID */
/* 7.28. ioctl VIDIOC_G_ENC_INDEX */
/* 7.29. ioctl VIDIOC_G_EXT_CTRLS, VIDIOC_S_EXT_CTRLS, VIDIOC_TRY_EXT_CTRLS */
/* 7.30. ioctl VIDIOC_G_FBUF, VIDIOC_S_FBUF */
/* 7.31. ioctl VIDIOC_G_FMT, VIDIOC_S_FMT, VIDIOC_TRY_FMT */

int g_v4l2_ioctl_get_format (gint                 fd,
                             GV4l2BufferTypes     type,
                             struct v4l2_format  *format,
                             GError             **error);
int g_v4l2_ioctl_try_format (gint                 fd,
                             struct v4l2_format  *format,
                             GError             **error);
int g_v4l2_ioctl_set_format (gint                 fd,
                             struct v4l2_format  *format,
                             GError             **error);

/* 7.32. ioctl VIDIOC_G_FREQUENCY, VIDIOC_S_FREQUENCY */
/* 7.33. ioctl VIDIOC_G_INPUT, VIDIOC_S_INPUT */
/* 7.34. ioctl VIDIOC_G_JPEGCOMP, VIDIOC_S_JPEGCOMP */
/* 7.35. ioctl VIDIOC_G_MODULATOR, VIDIOC_S_MODULATOR */
/* 7.36. ioctl VIDIOC_G_OUTPUT, VIDIOC_S_OUTPUT */
/* 7.37. ioctl VIDIOC_G_PARM, VIDIOC_S_PARM */
/* 7.38. ioctl VIDIOC_G_PRIORITY, VIDIOC_S_PRIORITY */
/* 7.39. ioctl VIDIOC_G_SELECTION, VIDIOC_S_SELECTION */
/* 7.40. ioctl VIDIOC_G_SLICED_VBI_CAP */
/* 7.41. ioctl VIDIOC_G_STD, VIDIOC_S_STD, VIDIOC_SUBDEV_G_STD, VIDIOC_SUBDEV_S_STD */
/* 7.42. ioctl VIDIOC_G_TUNER, VIDIOC_S_TUNER */
/* 7.43. ioctl VIDIOC_LOG_STATUS */
/* 7.44. ioctl VIDIOC_OVERLAY */
/* 7.45. ioctl VIDIOC_PREPARE_BUF */
/* 7.46. ioctl VIDIOC_QBUF, VIDIOC_DQBUF */

int g_v4l2_ioctl_queue_buffer   (gint                 fd,
                                 struct v4l2_buffer  *buffer,
                                 GError             **error);
int g_v4l2_ioctl_dequeue_buffer (gint                 fd,
                                 struct v4l2_buffer  *buffer,
                                 GError             **error);

/* 7.47. ioctl VIDIOC_QUERYBUF */

int g_v4l2_ioctl_query_buffer (gint                 fd,
                               struct v4l2_buffer  *buffer,
                               GError             **error);

/* 7.48. ioctl VIDIOC_QUERYCAP */

int g_v4l2_ioctl_query_capabilities (gint                     fd,
                                     struct v4l2_capability  *capability,
                                     GError                 **error);

/* 7.49. ioctls VIDIOC_QUERYCTRL, VIDIOC_QUERY_EXT_CTRL and VIDIOC_QUERYMENU */
/* 7.50. ioctl VIDIOC_QUERY_DV_TIMINGS */
/* 7.51. ioctl VIDIOC_QUERYSTD, VIDIOC_SUBDEV_QUERYSTD */
/* 7.52. ioctl VIDIOC_REQBUFS */

int g_v4l2_ioctl_request_buffers (gint                         fd,
                                  struct v4l2_requestbuffers  *reqbuf,
                                  GError                     **error);

/* 7.53. ioctl VIDIOC_S_HW_FREQ_SEEK */
/* 7.54. ioctl VIDIOC_STREAMON, VIDIOC_STREAMOFF */

int g_v4l2_ioctl_stream_on  (gint               fd,
                             GV4l2BufferTypes   type,
                             GError           **error);
int g_v4l2_ioctl_stream_off (gint               fd,
                             GV4l2BufferTypes   type,
                             GError           **error);

/* 7.55. ioctl VIDIOC_SUBDEV_ENUM_FRAME_INTERVAL */
/* 7.56. ioctl VIDIOC_SUBDEV_ENUM_FRAME_SIZE */
/* 7.57. ioctl VIDIOC_SUBDEV_ENUM_MBUS_CODE */
/* 7.58. ioctl VIDIOC_SUBDEV_G_CROP, VIDIOC_SUBDEV_S_CROP */
/* 7.59. ioctl VIDIOC_SUBDEV_G_FMT, VIDIOC_SUBDEV_S_FMT */
/* 7.60. ioctl VIDIOC_SUBDEV_G_FRAME_INTERVAL, VIDIOC_SUBDEV_S_FRAME_INTERVAL */
/* 7.61. ioctl VIDIOC_SUBDEV_G_SELECTION, VIDIOC_SUBDEV_S_SELECTION */
/* 7.62. ioctl VIDIOC_SUBDEV_QUERYCAP */
/* 7.63. ioctl VIDIOC_SUBSCRIBE_EVENT, VIDIOC_UNSUBSCRIBE_EVENT */

int g_v4l2_ioctl_subscribe_event   (gint      fd,
                                    guint32   type,
                                    guint32   id,
                                    guint32   flags,
                                    GError  **error);
int g_v4l2_ioctl_unsubscribe_event (gint      fd,
                                    guint32   type,
                                    guint32   id,
                                    guint32   flags,
                                    GError  **error);

G_END_DECLS
