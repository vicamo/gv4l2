/* gv4l2 - V4L2 abstraction library using GLib
 *
 * Copyright (C) 2020 Canonical Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details:
 */

#pragma once

#if !defined (__GV4L2_GV4L2_H_INSIDE__) && !defined (LIBGV4L2_COMPILATION)
#error "Only <gv4l2/gv4l2.h> can be included directly."
#endif

#include <glib.h>
#include <glib-object.h>
#include <gv4l2/gv4l2-fwd.h>
#include <gv4l2/gv4l2-ioctl.h>

G_BEGIN_DECLS


#define G_V4L2_TYPE_DEVICE  (g_v4l2_device_get_type ())

G_DECLARE_DERIVABLE_TYPE (GV4l2Device, g_v4l2_device, G_V4L2, DEVICE, GObject)

#define G_V4L2_DEVICE_PROP_FD "fd"
#define G_V4L2_DEVICE_PROP_INIT_FLAGS "init-flags"
#define G_V4L2_DEVICE_PROP_DRIVER_NAME "driver-name"
#define G_V4L2_DEVICE_PROP_DRIVER_VERSION "driver-version"
#define G_V4L2_DEVICE_PROP_BUS_INFO "bus-info"
#define G_V4L2_DEVICE_PROP_CARD_NAME "card-name"
#define G_V4L2_DEVICE_PROP_CAPABILITIES "capabilities"

#define G_V4L2_DEVICE_SIGNAL_EVENT "event"

struct _GV4l2DeviceClass {
  GObjectClass parent_class;

  /* signal callbacks */
  void (*event) (GV4l2Device             *device,
                 const struct v4l2_event *event);

  /* Reserved slots for furture extension. */
  gpointer padding[12];
};

typedef enum { /*< flags,prefix=G_V4L2_DEVICE_INIT,underscore_name=g_v4l2_device_init_flags >*/
  G_V4L2_DEVICE_INIT_NO_ATTACH_SOURCE,
  G_V4L2_DEVICE_INIT_V4L2LOOPBACK_WORKARROUND,
} GV4l2DeviceInitFlags;

/* Constructors */

GV4l2Device* g_v4l2_device_new_from_fd       (gint                   fd,
                                              GV4l2DeviceInitFlags   flags,
                                              GError               **error);
GV4l2Device* g_v4l2_device_new_from_filename (const gchar           *filename,
                                              GV4l2DeviceInitFlags   flags,
                                              GError               **error);

/* Properties */

gint                 g_v4l2_device_get_fd             (GV4l2Device *device);
GV4l2DeviceInitFlags g_v4l2_device_get_init_flags     (GV4l2Device *device);
const gchar*         g_v4l2_device_get_driver_name    (GV4l2Device *device);
guint                g_v4l2_device_get_driver_version (GV4l2Device *device);
const gchar*         g_v4l2_device_get_bus_info       (GV4l2Device *device);
const gchar*         g_v4l2_device_get_card_name      (GV4l2Device *device);
GV4l2Capabilities    g_v4l2_device_get_capabilities   (GV4l2Device *device);

int g_v4l2_device_subscribe_event   (GV4l2Device  *device,
                                     guint32       type,
                                     guint32       id,
                                     guint32       flags,
                                     GError      **error);
int g_v4l2_device_unsubscribe_event (GV4l2Device  *device,
                                     guint32       type,
                                     guint32       id,
                                     guint32       flags,
                                     GError      **error);

int g_v4l2_device_query_buffer    (GV4l2Device                 *device,
                                   struct v4l2_buffer          *buffer,
                                   GError                     **error);
int g_v4l2_device_queue_buffer    (GV4l2Device                 *device,
                                   struct v4l2_buffer          *buffer,
                                   GError                     **error);
int g_v4l2_device_dequeue_buffer  (GV4l2Device                 *device,
                                   struct v4l2_buffer          *buffer,
                                   GError                     **error);

int g_v4l2_device_stream_on  (GV4l2Device       *device,
                              GV4l2BufferTypes   type,
                              GError           **error);
int g_v4l2_device_stream_off (GV4l2Device       *device,
                              GV4l2BufferTypes   type,
                              GError           **error);

int g_v4l2_device_get_format (GV4l2Device         *device,
                              GV4l2BufferTypes     type,
                              struct v4l2_format  *format,
                              GError             **error);
int g_v4l2_device_try_format (GV4l2Device         *device,
                              struct v4l2_format  *format,
                              GError             **error);
int g_v4l2_device_set_format (GV4l2Device         *device,
                              struct v4l2_format  *format,
                              GError             **error);

int g_v4l2_device_start_streaming (GV4l2Device       *device,
                                   guint              buf_count,
                                   GV4l2BufferTypes   type,
                                   GV4l2MemoryTypes   memory,
                                   GError           **error);
int g_v4l2_device_stop_streaming  (GV4l2Device       *device);

G_END_DECLS
