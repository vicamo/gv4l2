/* gv4l2 - V4L2 abstraction library using GLib
 *
 * Copyright (C) 2020 Canonical Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details:
 */

#if defined (HAVE_CONFIG_H)
#include "config.h"
#endif

#include <gio/gio.h>

#include "gv4l2/gv4l2-enums.h"
#include "gv4l2/gv4l2-streamable.h"

struct _GV4l2StreamablePrivate {
  GV4l2BufferTypes buffer_type;
  GV4l2MemoryTypes memory_type;
  GV4l2BufferCapabilities capabilities;

  gboolean is_streaming;
};

G_DEFINE_INTERFACE (GV4l2Streamable, g_v4l2_streamable, G_TYPE_OBJECT)

static void
g_v4l2_streamable_default_init (GV4l2StreamableInterface *iface)
{
}

GV4l2StreamablePrivate*
g_v4l2_streamable_alloc_private ()
{
  return g_new0 (GV4l2StreamablePrivate, 1);
}

void
g_v4l2_streamable_free_private (GV4l2StreamablePrivate *priv)
{
  g_free (priv);
}

int
g_v4l2_streamable_reset (GV4l2Streamable   *streamable,
                         GV4l2BufferTypes   buffer_type,
                         GV4l2MemoryTypes   memory_type,
                         GError           **error)
{
  GV4l2StreamableInterface *iface;
  GV4l2StreamablePrivate *priv;
  struct v4l2_requestbuffers reqbuf;
  int fd, ret;

  g_return_val_if_fail (G_V4L2_IS_STREAMABLE (streamable), -1);

  iface = G_V4L2_STREAMABLE_GET_IFACE (streamable);
  fd = iface->get_fd (streamable);
  priv = iface->get_private (streamable);

  memset (&reqbuf, 0, sizeof(reqbuf));
  reqbuf.count = 0;
  reqbuf.type = buffer_type;
  reqbuf.memory = memory_type;
  ret = g_v4l2_ioctl_request_buffers (fd, &reqbuf, error);
  if (ret)
    return ret;

  priv->buffer_type = reqbuf.type;
  priv->memory_type = reqbuf.memory;
  priv->capabilities = reqbuf.capabilities;

  return ret;
}

GV4l2BufferTypes
g_v4l2_streamable_get_buffer_type (GV4l2Streamable *streamable)
{
  GV4l2StreamableInterface *iface;
  GV4l2StreamablePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_STREAMABLE (streamable), 0);

  iface = G_V4L2_STREAMABLE_GET_IFACE (streamable);
  priv = iface->get_private (streamable);
  return priv->buffer_type;
}

GV4l2MemoryTypes
g_v4l2_streamable_get_memory_type (GV4l2Streamable *streamable)
{
  GV4l2StreamableInterface *iface;
  GV4l2StreamablePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_STREAMABLE (streamable), 0);

  iface = G_V4L2_STREAMABLE_GET_IFACE (streamable);
  priv = iface->get_private (streamable);
  return priv->memory_type;
}

GV4l2BufferCapabilities
g_v4l2_streamable_get_capabilities (GV4l2Streamable *streamable)
{
  GV4l2StreamableInterface *iface;
  GV4l2StreamablePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_STREAMABLE (streamable), 0);

  iface = G_V4L2_STREAMABLE_GET_IFACE (streamable);
  priv = iface->get_private (streamable);
  return priv->capabilities;
}

int
g_v4l2_streamable_request_buffers (GV4l2Streamable  *streamable,
                                   guint            *n_buffers,
                                   GError          **error)
{
  GV4l2StreamableInterface *iface;
  GV4l2StreamablePrivate *priv;
  struct v4l2_requestbuffers reqbuf;
  int fd, ret;

  g_return_val_if_fail (G_V4L2_IS_STREAMABLE (streamable) && n_buffers, -1);

  iface = G_V4L2_STREAMABLE_GET_IFACE (streamable);
  priv = iface->get_private (streamable);

  if (!priv->buffer_type) {
    g_set_error (error, G_FILE_ERROR, G_FILE_ERROR_INVAL,
                 "Invalid buffer type");
    return -EINVAL;
  }

  memset (&reqbuf, 0, sizeof(reqbuf));
  reqbuf.count = *n_buffers;
  reqbuf.type = priv->buffer_type;
  reqbuf.memory = priv->memory_type;

  fd = iface->get_fd (streamable);
  ret = g_v4l2_ioctl_request_buffers (fd, &reqbuf, error);
  if (ret)
    return ret;

  *n_buffers = reqbuf.count;
  return ret;
}

int
g_v4l2_streamable_get_nth_buffer (GV4l2Streamable  *streamable,
                                  guint             nth,
                                  GError          **error)
{
  GV4l2StreamableInterface *iface;
  GV4l2StreamablePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_STREAMABLE (streamable), -1);

  iface = G_V4L2_STREAMABLE_GET_IFACE (streamable);
  priv = iface->get_private (streamable);

  if (!priv->buffer_type) {
    g_set_error (error, G_FILE_ERROR, G_FILE_ERROR_INVAL,
                 "Invalid buffer type");
    return -EINVAL;
  }

  return 0;
}

int
g_v4l2_streamable_put_nth_buffer (GV4l2Streamable  *streamable,
                                  guint             nth,
                                  GError          **error)
{
  return 0;
}

int
g_v4l2_streamable_stream_on (GV4l2Streamable  *streamable,
                             GError          **error)
{
  GV4l2StreamableInterface *iface;
  GV4l2StreamablePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_STREAMABLE (streamable), -1);

  iface = G_V4L2_STREAMABLE_GET_IFACE (streamable);
  priv = iface->get_private (streamable);

  priv->is_streaming = TRUE;
  return 0;
}

int
g_v4l2_streamable_stream_off (GV4l2Streamable  *streamable,
                              GError          **error)
{
  GV4l2StreamableInterface *iface;
  GV4l2StreamablePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_STREAMABLE (streamable), -1);

  iface = G_V4L2_STREAMABLE_GET_IFACE (streamable);
  priv = iface->get_private (streamable);

  priv->is_streaming = FALSE;
  return 0;
}
