/* gv4l2 - V4L2 abstraction library using GLib
 *
 * Copyright (C) 2020 Canonical Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details:
 */

#if defined (HAVE_CONFIG_H)
#include "config.h"
#endif

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include <glib.h>
#include <glib/gstdio.h>
#include <glib-unix.h>
#include <gio/gio.h>

#include "gv4l2/gv4l2-device.h"
#include "gv4l2/gv4l2-enums.h"
#include "gv4l2/gv4l2-streamable.h"

typedef struct _GV4l2DevicePrivate {
  int fd;
  GV4l2DeviceInitFlags init_flags;
  struct v4l2_capability capabilities;
  __u32 device_caps;

  /* GV4l2Streamable */
  GV4l2StreamablePrivate *streamable_priv;
  guint source_id;
} GV4l2DevicePrivate;

static void g_v4l2_device_streamable_iface_init (GV4l2StreamableInterface *iface);

G_DEFINE_TYPE_WITH_CODE (GV4l2Device, g_v4l2_device, G_TYPE_OBJECT,
                         G_ADD_PRIVATE (GV4l2Device)
                         G_IMPLEMENT_INTERFACE (G_V4L2_TYPE_STREAMABLE,
                                                g_v4l2_device_streamable_iface_init))

enum
{
  PROP_0,
  PROP_FD,
  PROP_INIT_FLAGS,
  PROP_DRIVER_NAME,
  PROP_DRIVER_VERSION,
  PROP_CARD_NAME,
  PROP_BUS_INFO,
  PROP_CAPABILITIES,
  N_PROPERTIES
};

static GParamSpec *props[N_PROPERTIES] = { NULL, };

enum
{
  SIGNAL_EVENT,
  N_SIGNALS
};

static guint signals[N_SIGNALS] = { 0 };

static gboolean on_source_callback (gint         fd,
                                    GIOCondition condition,
                                    gpointer     user_data);

static void
gobject_class_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  GV4l2Device *device = G_V4L2_DEVICE (object);
  GV4l2DevicePrivate *priv = g_v4l2_device_get_instance_private (device);

  switch (prop_id) {
    case PROP_FD:
      priv->fd = g_value_get_int (value);
      break;
    case PROP_INIT_FLAGS:
      priv->init_flags = g_value_get_flags (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gobject_class_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  GV4l2Device *device = G_V4L2_DEVICE (object);
  GV4l2DevicePrivate *priv = g_v4l2_device_get_instance_private (device);

  switch (prop_id) {
    case PROP_FD:
      g_value_set_int (value, priv->fd);
      break;
    case PROP_INIT_FLAGS:
      g_value_set_flags (value, priv->init_flags);
      break;
    case PROP_DRIVER_NAME:
      g_value_set_string (value, (const gchar*) priv->capabilities.driver);
      break;
    case PROP_DRIVER_VERSION:
      g_value_set_uint (value, priv->capabilities.version);
      break;
    case PROP_CARD_NAME:
      g_value_set_string (value, (const gchar*) priv->capabilities.card);
      break;
    case PROP_BUS_INFO:
      g_value_set_string (value, (const gchar*) priv->capabilities.bus_info);
      break;
    case PROP_CAPABILITIES:
      g_value_set_flags (value, priv->device_caps);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gobject_class_dispose (GObject *object)
{
  GV4l2Device *device = G_V4L2_DEVICE (object);
  GV4l2DevicePrivate *priv = g_v4l2_device_get_instance_private (device);

  if (priv->source_id) {
    g_source_remove (priv->source_id);
    priv->source_id = 0;
  }
}

static void
gobject_class_finalize (GObject *object)
{
  GV4l2Device *device = G_V4L2_DEVICE (object);
  GV4l2DevicePrivate *priv = g_v4l2_device_get_instance_private (device);

  if (priv->fd >= 0) {
    g_close (priv->fd, NULL);
    priv->fd = -1;
  }

  if (priv->streamable_priv != NULL) {
    g_v4l2_streamable_free_private (priv->streamable_priv);
    priv->streamable_priv = NULL;
  }
}

static void
gobject_class_constructed (GObject *object)
{
  GV4l2Device *device = G_V4L2_DEVICE (object);
  GV4l2DevicePrivate *priv = g_v4l2_device_get_instance_private (device);
  struct v4l2_capability *caps;
  GIOCondition condition;
  GError *error = NULL;

  if (priv->fd < 0)
    return;

  caps = &priv->capabilities;
  if (g_v4l2_ioctl_query_capabilities (priv->fd, caps, &error) < 0) {
    if (error != NULL) {
      g_debug ("%s", error->message);
      g_error_free (error);
    }
    return;
  }

#if defined(V4L2_CAP_DEVICE_CAPS)
  if (caps->capabilities & V4L2_CAP_DEVICE_CAPS) {
    priv->device_caps = caps->device_caps;
  } else
#endif
    priv->device_caps = caps->capabilities;

  if (!(priv->init_flags & G_V4L2_DEVICE_INIT_NO_ATTACH_SOURCE)) {
    condition = G_IO_ERR | G_IO_HUP;
    condition |= G_IO_PRI; /* used by V4L2 Event API */
    if (!(priv->init_flags & G_V4L2_DEVICE_INIT_V4L2LOOPBACK_WORKARROUND)) {
      /* v4l2loopback cannot do poll at OUTPUT correctly */
      if (priv->device_caps & V4L2_CAP_READWRITE) {
        if (priv->device_caps & V4L2_CAP_VIDEO_OUTPUT)
          condition |= G_IO_OUT;
        if (priv->device_caps & V4L2_CAP_VIDEO_CAPTURE)
          condition |= G_IO_IN;
      }
    }

    priv->source_id =
        g_unix_fd_add (priv->fd, condition, on_source_callback, device);
  }
}

static void
g_v4l2_device_class_init (GV4l2DeviceClass *device_class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (device_class);

  /* virtual methods */

  object_class->set_property = gobject_class_set_property;
  object_class->get_property = gobject_class_get_property;
  object_class->dispose = gobject_class_dispose;
  object_class->finalize = gobject_class_finalize;
  object_class->constructed = gobject_class_constructed;

  /* signals */

  signals[SIGNAL_EVENT] =
      g_signal_new (G_V4L2_DEVICE_SIGNAL_EVENT,
                    G_TYPE_FROM_CLASS (device_class),
                    G_SIGNAL_RUN_LAST,
                    G_STRUCT_OFFSET (GV4l2DeviceClass, event),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__POINTER,
                    G_TYPE_NONE, 1, G_TYPE_POINTER);

  /* properties */

  props[PROP_FD] =
      g_param_spec_int (G_V4L2_DEVICE_PROP_FD, "fd", "File descriptor",
                        -1, G_MAXINT, -1,
                        G_PARAM_CONSTRUCT_ONLY | \
                            G_PARAM_READWRITE | \
                            G_PARAM_STATIC_STRINGS);

  props[PROP_INIT_FLAGS] =
      g_param_spec_flags (G_V4L2_DEVICE_PROP_INIT_FLAGS, "init-flags", "Init flags",
                          G_TYPE_V4L2_DEVICE_INIT_FLAGS, 0,
                          G_PARAM_CONSTRUCT_ONLY | \
                              G_PARAM_READWRITE | \
                              G_PARAM_STATIC_STRINGS);

  props[PROP_DRIVER_NAME] =
      g_param_spec_string (G_V4L2_DEVICE_PROP_DRIVER_NAME,
                           "driver-name", "Driver name",
                           NULL,
                           G_PARAM_READABLE | \
                               G_PARAM_STATIC_STRINGS);

  props[PROP_DRIVER_VERSION] =
      g_param_spec_uint (G_V4L2_DEVICE_PROP_DRIVER_VERSION,
                         "driver-version", "Driver version",
                         0, G_MAXUINT, 0,
                         G_PARAM_READABLE | \
                             G_PARAM_STATIC_STRINGS);

  props[PROP_CARD_NAME] =
      g_param_spec_string (G_V4L2_DEVICE_PROP_CARD_NAME,
                           "card-name", "Card name",
                           NULL,
                           G_PARAM_READABLE | \
                               G_PARAM_STATIC_STRINGS);

  props[PROP_BUS_INFO] =
      g_param_spec_string (G_V4L2_DEVICE_PROP_BUS_INFO,
                           "bus-info", "Bus information",
                           NULL,
                           G_PARAM_READABLE | \
                               G_PARAM_STATIC_STRINGS);

  props[PROP_CAPABILITIES] =
      g_param_spec_flags (G_V4L2_DEVICE_PROP_CAPABILITIES,
                          "capabilities", "Device capabilities",
                          G_TYPE_V4L2_CAPABILITIES, 0,
                          G_PARAM_READABLE | \
                              G_PARAM_STATIC_STRINGS);

  g_object_class_install_properties (object_class, N_PROPERTIES, props);
}

static void
g_v4l2_device_init (GV4l2Device *device)
{
  GV4l2DevicePrivate *priv = g_v4l2_device_get_instance_private (device);

  priv->fd = -1;
  priv->streamable_priv = g_v4l2_streamable_alloc_private ();
}

static GV4l2StreamablePrivate*
device_streamable_get_private (GV4l2Streamable *streamable)
{
  GV4l2Device *device = G_V4L2_DEVICE (streamable);
  GV4l2DevicePrivate *priv = g_v4l2_device_get_instance_private (device);

  return priv->streamable_priv;
}

static gint
device_streamable_get_fd (GV4l2Streamable *streamable)
{
  return g_v4l2_device_get_fd (G_V4L2_DEVICE (streamable));
}

static void
g_v4l2_device_streamable_iface_init (GV4l2StreamableInterface *iface)
{
  iface->get_private = device_streamable_get_private;
  iface->get_fd = device_streamable_get_fd;
}

GV4l2Device*
g_v4l2_device_new_from_fd (gint                   fd,
                           GV4l2DeviceInitFlags   flags,
                           GError               **error G_GNUC_UNUSED)
{
  return g_object_new (G_V4L2_TYPE_DEVICE, G_V4L2_DEVICE_PROP_FD, fd,
                       G_V4L2_DEVICE_PROP_INIT_FLAGS, flags, NULL);
}

GV4l2Device*
g_v4l2_device_new_from_filename (const gchar           *filename,
                                 GV4l2DeviceInitFlags   flags,
                                 GError               **error)
{
  gint fd, saved_errno;

  fd = g_open (filename, O_NONBLOCK | O_RDWR, 0);
  if (fd < 0) {
    saved_errno = errno;
    g_set_error (error, G_FILE_ERROR, g_file_error_from_errno (saved_errno),
                 "could not open device '%s': %s",
                 filename, g_strerror (saved_errno));
    return NULL;
  }

  return g_v4l2_device_new_from_fd (fd, flags, error);
}

gint
g_v4l2_device_get_fd (GV4l2Device *device)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), -1);

  priv = g_v4l2_device_get_instance_private (device);
  return priv->fd;
}

GV4l2DeviceInitFlags
g_v4l2_device_get_init_flags (GV4l2Device *device)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), 0);

  priv = g_v4l2_device_get_instance_private (device);
  return priv->init_flags;
}

const gchar*
g_v4l2_device_get_driver_name (GV4l2Device *device)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), NULL);

  priv = g_v4l2_device_get_instance_private (device);
  return (const gchar*) priv->capabilities.driver;
}

guint
g_v4l2_device_get_driver_version (GV4l2Device *device)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), 0);

  priv = g_v4l2_device_get_instance_private (device);
  return priv->capabilities.version;
}

const gchar*
g_v4l2_device_get_bus_info (GV4l2Device *device)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), NULL);

  priv = g_v4l2_device_get_instance_private (device);
  return (const gchar*) priv->capabilities.bus_info;
}

const gchar*
g_v4l2_device_get_card_name (GV4l2Device *device)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), NULL);

  priv = g_v4l2_device_get_instance_private (device);
  return (const gchar*) priv->capabilities.card;
}

GV4l2Capabilities
g_v4l2_device_get_capabilities (GV4l2Device *device)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), 0);

  priv = g_v4l2_device_get_instance_private (device);
  return priv->device_caps;
}

int
g_v4l2_device_subscribe_event (GV4l2Device  *device,
                               guint32       type,
                               guint32       id,
                               guint32       flags,
                               GError      **error)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), -1);

  priv = g_v4l2_device_get_instance_private (device);
  return g_v4l2_ioctl_subscribe_event (priv->fd, type, id, flags, error);
}

int
g_v4l2_device_unsubscribe_event (GV4l2Device  *device,
                                 guint32       type,
                                 guint32       id,
                                 guint32       flags,
                                 GError      **error)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), -1);

  priv = g_v4l2_device_get_instance_private (device);
  return g_v4l2_ioctl_unsubscribe_event (priv->fd, type, id, flags, error);
}

static void
handle_event (GV4l2Device *device)
{
  GV4l2DevicePrivate *priv;
  struct v4l2_event event;
  int ret;

  priv = g_v4l2_device_get_instance_private (device);

  do {
    ret = g_v4l2_ioctl_dequeue_event (priv->fd, &event, NULL);
    if (ret < 0)
      return;

    g_debug ("Received event seq %u type %s id %u pending %u ts %lu.%lu",
             event.sequence, g_v4l2_events_get_string (event.type),
             event.id, event.pending, event.timestamp.tv_sec, event.timestamp.tv_nsec);
    g_signal_emit (device, signals[SIGNAL_EVENT], 0, &event);
  } while (event.pending);
}

static gboolean
on_source_callback (gint         fd,
                    GIOCondition condition,
                    gpointer     user_data)
{
  GV4l2Device *device = (GV4l2Device*) user_data;

  g_debug ("on_source_callback(%d, %x, %p)", fd, condition, user_data);

  if (condition & G_IO_PRI)
    handle_event (device);

  return TRUE;
}

int
g_v4l2_device_query_buffer (GV4l2Device         *device,
                            struct v4l2_buffer  *buffer,
                            GError             **error)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), -1);

  priv = g_v4l2_device_get_instance_private (device);
  return g_v4l2_ioctl_query_buffer (priv->fd, buffer, error);
}

int
g_v4l2_device_queue_buffer (GV4l2Device         *device,
                            struct v4l2_buffer  *buffer,
                            GError             **error)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), -1);

  priv = g_v4l2_device_get_instance_private (device);
  return g_v4l2_ioctl_queue_buffer (priv->fd, buffer, error);
}

int
g_v4l2_device_dequeue_buffer (GV4l2Device         *device,
                              struct v4l2_buffer  *buffer,
                              GError             **error)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), -1);

  priv = g_v4l2_device_get_instance_private (device);
  return g_v4l2_ioctl_dequeue_buffer (priv->fd, buffer, error);
}

int
g_v4l2_device_stream_on (GV4l2Device       *device,
                         GV4l2BufferTypes   type,
                         GError           **error)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), -1);

  priv = g_v4l2_device_get_instance_private (device);
  return g_v4l2_ioctl_stream_on (priv->fd, type, error);
}

int
g_v4l2_device_stream_off (GV4l2Device       *device,
                          GV4l2BufferTypes   type,
                          GError           **error)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), -1);

  priv = g_v4l2_device_get_instance_private (device);
  return g_v4l2_ioctl_stream_on (priv->fd, type, error);
}

int
g_v4l2_device_get_format (GV4l2Device         *device,
                          GV4l2BufferTypes     type,
                          struct v4l2_format  *format,
                          GError             **error)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), -1);

  priv = g_v4l2_device_get_instance_private (device);
  return g_v4l2_ioctl_get_format (priv->fd, type, format, error);
}

int
g_v4l2_device_try_format (GV4l2Device         *device,
                          struct v4l2_format  *format,
                          GError             **error)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), -1);

  priv = g_v4l2_device_get_instance_private (device);
  return g_v4l2_ioctl_try_format (priv->fd, format, error);
}

int
g_v4l2_device_set_format (GV4l2Device         *device,
                          struct v4l2_format  *format,
                          GError             **error)
{
  GV4l2DevicePrivate *priv;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), -1);

  priv = g_v4l2_device_get_instance_private (device);
  return g_v4l2_ioctl_set_format (priv->fd, format, error);
}

typedef struct {
  guint index;
  guint length;
  gpointer start;
  struct v4l2_buffer buffer;
} MmapInfo;

static void
mmap_info_free (MmapInfo *info)
{
  if (info->start != NULL)
    munmap (info->start, info->length);
  g_slice_free (MmapInfo, info);
}

typedef struct {
  GSource _source;

  GV4l2Device *device;
  GQueue available; /* MmapInfo */
  GQueue used; /* MmapInfo */
} OutputMmapSource;

typedef gint (*OutputMmapSourceFunc) (const struct v4l2_buffer *buffer,
                                      gpointer                  user_data);

static gint
find_matching_mmapinfo_index (const MmapInfo *info,
                              gpointer        user_data)
{
  return info->index == GPOINTER_TO_UINT(user_data) ? 0 : -1;
}

static gboolean
outout_mmap_source_check (GSource *source)
{
  OutputMmapSource *omsource = (OutputMmapSource*) source;
  struct v4l2_buffer buffer;
  GError *error = NULL;
  GList *found;
  int ret;

  while (!g_queue_is_empty (&omsource->used)) {
    memset (&buffer, 0, sizeof(buffer));
    buffer.type = G_V4L2_BUF_TYPE_VIDEO_OUTPUT;
    buffer.memory = G_V4L2_MEMORY_MMAP;
    ret = g_v4l2_device_dequeue_buffer (omsource->device, &buffer, &error);
    if (ret < 0) {
      if (g_error_matches (error, G_FILE_ERROR, G_FILE_ERROR_AGAIN))
        break;

      g_debug ("Failed to dequeue buffer: %s", error->message);
      break;
    }

    found = g_queue_find_custom (&omsource->used,
                                 (GCompareFunc) find_matching_mmapinfo_index,
                                 GUINT_TO_POINTER (buffer.index));
    g_queue_unlink (&omsource->used, found);
    g_queue_push_tail_link (&omsource->available, found);
    g_assert_true (buffer.index == ((MmapInfo*)found->data)->buffer.index);
    ((MmapInfo*)found->data)->buffer = buffer;
  }

  if (error)
    g_error_free (error);

  return !g_queue_is_empty (&omsource->available);
}

static gboolean
outout_mmap_source_dispatch (GSource     *source,
                             GSourceFunc  callback,
                             gpointer     user_data)
{
  OutputMmapSource *omsource = (OutputMmapSource*) source;
  OutputMmapSourceFunc func = (OutputMmapSourceFunc)callback;
  struct v4l2_buffer buffer;
  GError *error = NULL;
  MmapInfo *info;
  int ret;

  while (!g_queue_is_empty (&omsource->available)) {
    info = g_queue_peek_head (&omsource->available);
    if (func)
      ret = func (&info->buffer, user_data);
    else
      ret = 0;
    if (ret < 0)
      return G_SOURCE_REMOVE;
    if (ret > 0)
      break;

    memset (&buffer, 0, sizeof(buffer));
    buffer.type = info->buffer.type;
    buffer.memory = info->buffer.memory;
    buffer.index = info->buffer.index;
    if (g_v4l2_device_queue_buffer (omsource->device, &buffer, &error) < 0) {
      g_debug ("Failed to enqueue processed buffer: %s", error->message);
      g_error_free (error);
      return G_SOURCE_REMOVE;
    }

    /* move available head to end of the used list */
    g_queue_pop_head (&omsource->available);
    g_queue_push_tail (&omsource->used, info);
  }

  return G_SOURCE_CONTINUE;
}

static GSourceFuncs output_mmap_source_funcs = {
  .check = &outout_mmap_source_check,
  .dispatch = &outout_mmap_source_dispatch,
};

static void
output_mmap_source_dispose (GSource *source)
{
  OutputMmapSource *omsource = (OutputMmapSource*) source;

  g_queue_clear_full (&omsource->available, (GDestroyNotify) mmap_info_free);
  g_queue_clear_full (&omsource->used, (GDestroyNotify) mmap_info_free);

  g_v4l2_device_stream_off (omsource->device, G_V4L2_BUF_TYPE_VIDEO_OUTPUT, NULL);
}

static GSource*
output_mmap_source_new (GV4l2Device  *device,
                        guint         buf_count,
                        GError      **error)
{
  GV4l2DevicePrivate *priv = g_v4l2_device_get_instance_private (device);
  OutputMmapSource *omsource;
  int ret;
  guint index;

  omsource = (OutputMmapSource*) g_source_new (&output_mmap_source_funcs,
                                               sizeof(OutputMmapSource));
  omsource->device = device;
  g_queue_init (&omsource->available);
  g_queue_init (&omsource->used);
  g_source_set_dispose_function ((GSource*) omsource,
                                 output_mmap_source_dispose);

#define MUST_SUCCEED(x) { ret = (x); if (ret < 0) goto out; }

  MUST_SUCCEED(g_v4l2_streamable_request_buffers (G_V4L2_STREAMABLE (device),
                                                  &buf_count, error))

  for (index = 0; index < buf_count; index++) {
    struct v4l2_buffer buffer;

    memset (&buffer, 0, sizeof(buffer));
    buffer.type = G_V4L2_BUF_TYPE_VIDEO_OUTPUT;
    buffer.memory = G_V4L2_MEMORY_MMAP;
    buffer.index = index;
    MUST_SUCCEED(g_v4l2_device_query_buffer (device, &buffer, error))

    MmapInfo *info = g_slice_alloc0 (sizeof(MmapInfo));
    info->index = index;
    info->length = buffer.length;
    info->start = mmap (NULL, buffer.length, PROT_READ | PROT_WRITE, MAP_SHARED,
                        priv->fd, buffer.m.offset);
    if (info->start == MAP_FAILED) {
      g_slice_free (MmapInfo, info);
      goto out;
    }

    g_queue_push_tail (&omsource->available, info);
  }

  for (index = 0; index < buf_count; index++) {
    struct v4l2_buffer buffer;
    gpointer info;

    memset (&buffer, 0, sizeof(buffer));
    buffer.type = G_V4L2_BUF_TYPE_VIDEO_OUTPUT;
    buffer.memory = G_V4L2_MEMORY_MMAP;
    buffer.index = index;
    MUST_SUCCEED(g_v4l2_device_queue_buffer (device, &buffer, error))

    /* move available head to end of the used list */
    info = g_queue_pop_head (&omsource->available);
    g_queue_push_tail (&omsource->used, info);
  }

  MUST_SUCCEED(g_v4l2_device_stream_on (device, G_V4L2_BUF_TYPE_VIDEO_OUTPUT, error))

  return (GSource*) omsource;

out:
  g_source_unref ((GSource*) omsource);
  return NULL;
}

int
g_v4l2_device_start_streaming (GV4l2Device       *device,
                               guint              buf_count,
                               GV4l2BufferTypes   type,
                               GV4l2MemoryTypes   memory,
                               GError           **error)
{
  OutputMmapSource *omsource;

  g_return_val_if_fail (G_V4L2_IS_DEVICE (device), -1);

  switch (type) {
  case G_V4L2_BUF_TYPE_VIDEO_OUTPUT:
  case G_V4L2_BUF_TYPE_VIDEO_CAPTURE:
    break;
  default:
    g_set_error (error, G_FILE_ERROR, G_FILE_ERROR_NOSYS, "Not implemented");
    return -1;
  }

  switch (memory) {
  case G_V4L2_MEMORY_MMAP:
    break;
  default:
    g_set_error (error, G_FILE_ERROR, G_FILE_ERROR_NOSYS, "Not implemented");
    return -1;
  }

  omsource =
      (OutputMmapSource*) output_mmap_source_new (device, buf_count, error);
  if (omsource == NULL)
    return -1;

  return 0;
}

int
g_v4l2_device_stop_streaming (GV4l2Device *device)
{
  return 0;
}
