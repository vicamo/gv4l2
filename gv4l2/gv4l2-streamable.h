/* gv4l2 - V4L2 abstraction library using GLib
 *
 * Copyright (C) 2020 Canonical Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details:
 */

#pragma once

#if !defined (__GV4L2_GV4L2_H_INSIDE__) && !defined (LIBGV4L2_COMPILATION)
#error "Only <gv4l2/gv4l2.h> can be included directly."
#endif

#include <glib-object.h>
#include <gv4l2/gv4l2-fwd.h>

G_BEGIN_DECLS

#define G_V4L2_TYPE_STREAMABLE  (g_v4l2_streamable_get_type ())

G_DECLARE_INTERFACE (GV4l2Streamable, g_v4l2_streamable, G_V4L2, STREAMABLE,
                     GObject)

typedef struct _GV4l2StreamablePrivate GV4l2StreamablePrivate;

struct _GV4l2StreamableInterface {
  GTypeInterface parent_iface;

  GV4l2StreamablePrivate* (*get_private) (GV4l2Streamable *streamable);
  gint                    (*get_fd)      (GV4l2Streamable *streamable);

  /* Reserved slots for furture extension. */
  gpointer padding[12];
};

GV4l2StreamablePrivate* g_v4l2_streamable_alloc_private ();
void                    g_v4l2_streamable_free_private (GV4l2StreamablePrivate *priv);

int g_v4l2_streamable_reset (GV4l2Streamable   *streamable,
                             GV4l2BufferTypes   buffer_type,
                             GV4l2MemoryTypes   memory_type,
                             GError           **error);

GV4l2BufferTypes        g_v4l2_streamable_get_buffer_type  (GV4l2Streamable *streamable);
GV4l2MemoryTypes        g_v4l2_streamable_get_memory_type  (GV4l2Streamable *streamable);
GV4l2BufferCapabilities g_v4l2_streamable_get_capabilities (GV4l2Streamable *streamable);

int g_v4l2_streamable_request_buffers (GV4l2Streamable  *streamable,
                                       guint            *n_buffers,
                                       GError          **error);
int g_v4l2_streamable_get_nth_buffer  (GV4l2Streamable  *streamable,
                                       guint             nth,
                                       GError          **error);
int g_v4l2_streamable_put_nth_buffer  (GV4l2Streamable  *streamable,
                                       guint             nth,
                                       GError          **error);

int g_v4l2_streamable_stream_on  (GV4l2Streamable  *streamable,
                                  GError          **error);
int g_v4l2_streamable_stream_off (GV4l2Streamable  *streamable,
                                  GError          **error);

G_END_DECLS
