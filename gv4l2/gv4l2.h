/* gv4l2 - V4L2 abstraction library using GLib
 *
 * Copyright (C) 2020 Canonical Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details:
 */

#pragma once

#define __GV4L2_GV4L2_H_INSIDE__

#include <gv4l2/gv4l2-version.h>

#include <gv4l2/gv4l2-fwd.h>
#include <gv4l2/gv4l2-device.h>
#include <gv4l2/gv4l2-enums.h>
#include <gv4l2/gv4l2-ioctl.h>
#include <gv4l2/gv4l2-streamable.h>

#undef __GV4L2_GV4L2_H_INSIDE__
