/* gv4l2 - V4L2 abstraction library using GLib
 *
 * Copyright (C) 2020 Canonical Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details:
 */

#if defined (HAVE_CONFIG_H)
#include "config.h"
#endif

#include <errno.h>
#include <sys/ioctl.h>

#include "gv4l2/gv4l2-ioctl.h"

static int
do_ioctl (int           fd,
          const gchar  *req_str,
          gulong        request,
          gpointer      data,
          const gchar  *format,
          GError      **error)
{
  int ret, saved_errno;

  ret = ioctl (fd, request, data);
  saved_errno = errno;

  g_debug ("ioctl(%d, %s, %p) = %d", fd, req_str, data, ret);

  if (ret < 0) {
    g_set_error (error, G_FILE_ERROR, g_file_error_from_errno (saved_errno),
                 format, g_strerror (saved_errno));

    return -saved_errno;
  }

  return 0;
}

int
g_v4l2_ioctl_dequeue_event (gint                fd,
                            struct v4l2_event  *event,
                            GError            **error)
{
  g_return_val_if_fail (event != NULL, -1);

  memset (event, 0, sizeof(*event));
  return do_ioctl (fd, "VIDIOC_DQEVENT", VIDIOC_DQEVENT, event,
                   "Could not get buffer format: %s\n", error);
}

int
g_v4l2_ioctl_get_format (gint                 fd,
                         GV4l2BufferTypes     type,
                         struct v4l2_format  *format,
                         GError             **error)
{
  g_return_val_if_fail (format != NULL, -1);

  memset (format, 0, sizeof(*format));
  format->type = type;

  return do_ioctl (fd, "VIDIOC_G_FMT", VIDIOC_G_FMT, format,
                   "Could not get buffer format: %s\n", error);
}

int
g_v4l2_ioctl_try_format (gint                 fd,
                         struct v4l2_format  *format,
                         GError             **error)
{
  g_return_val_if_fail (format != NULL, -1);

  return do_ioctl (fd, "VIDIOC_TRY_FMT", VIDIOC_TRY_FMT, format,
                   "Invalid format: %s\n", error);
}

int
g_v4l2_ioctl_set_format (gint                 fd,
                         struct v4l2_format  *format,
                         GError             **error)
{
  g_return_val_if_fail (format != NULL, -1);

  return do_ioctl (fd, "VIDIOC_S_FMT", VIDIOC_S_FMT, format,
                   "Invalid format: %s\n", error);
}

int
g_v4l2_ioctl_queue_buffer (gint                 fd,
                           struct v4l2_buffer  *buffer,
                           GError             **error)
{
  g_return_val_if_fail (buffer != NULL, -1);

  return do_ioctl (fd, "VIDIOC_QBUF", VIDIOC_QBUF, buffer,
                   "Could not query buffer: %s\n", error);
}

int
g_v4l2_ioctl_dequeue_buffer (gint                 fd,
                             struct v4l2_buffer  *buffer,
                             GError             **error)
{
  g_return_val_if_fail (buffer != NULL, -1);

  return do_ioctl (fd, "VIDIOC_DQBUF", VIDIOC_DQBUF, buffer,
                   "Could not dequery buffer: %s\n", error);
}

int
g_v4l2_ioctl_query_buffer (gint                 fd,
                           struct v4l2_buffer  *buffer,
                           GError             **error)
{
  g_return_val_if_fail (buffer != NULL, -1);

  return do_ioctl (fd, "VIDIOC_QUERYBUF", VIDIOC_QUERYBUF, buffer,
                   "Could not query buffer: %s\n", error);
}

int
g_v4l2_ioctl_query_capabilities (gint                     fd,
                                 struct v4l2_capability  *capability,
                                 GError                 **error)
{
  g_return_val_if_fail (capability != NULL, -1);

  memset (capability, 0, sizeof(*capability));
  return do_ioctl (fd, "VIDIOC_QUERYCAP", VIDIOC_QUERYCAP, capability,
                   "Could not query capabilities: %s\n", error);
}

int
g_v4l2_ioctl_request_buffers (gint                         fd,
                              struct v4l2_requestbuffers  *reqbuf,
                              GError                     **error)
{
  g_return_val_if_fail (reqbuf != NULL, -1);

  return do_ioctl (fd, "VIDIOC_REQBUFS", VIDIOC_REQBUFS, reqbuf,
                   "Could not request buffer: %s\n", error);
}

int
g_v4l2_ioctl_stream_on (gint               fd,
                        GV4l2BufferTypes   type,
                        GError           **error)
{
  int data = type;

  return do_ioctl (fd, "VIDIOC_STREAMON", VIDIOC_STREAMON, &data,
                   "Could not streamon: %s\n", error);
}

int
g_v4l2_ioctl_stream_off (gint               fd,
                         GV4l2BufferTypes   type,
                         GError           **error)
{
  int data = type;

  return do_ioctl (fd, "VIDIOC_STREAMOFF", VIDIOC_STREAMOFF, &data,
                   "Could not streamoff: %s\n", error);
}

int
g_v4l2_ioctl_subscribe_event (gint      fd,
                              guint32   type,
                              guint32   id,
                              guint32   flags,
                              GError  **error)
{
  struct v4l2_event_subscription subscription;

  memset (&subscription, 0, sizeof(subscription));
  subscription.type = type;
  subscription.id = id;
  subscription.flags = flags;

  return do_ioctl (fd, "VIDIOC_SUBSCRIBE_EVENT", VIDIOC_SUBSCRIBE_EVENT,
                   &subscription,
                   "Could not subscribe to event: %s\n", error);
}

int
g_v4l2_ioctl_unsubscribe_event (gint      fd,
                                guint32   type,
                                guint32   id,
                                guint32   flags,
                                GError  **error)
{
  struct v4l2_event_subscription subscription;

  memset (&subscription, 0, sizeof(subscription));
  subscription.type = type;
  subscription.id = id;
  subscription.flags = flags;

  return do_ioctl (fd, "VIDIOC_UNSUBSCRIBE_EVENT", VIDIOC_UNSUBSCRIBE_EVENT,
                   &subscription,
                   "Could not unsubscribe to event: %s\n", error);
}
